package main

import (
	"log"
	"os"
	"os/signal"

	"gitlab.com/livesocket/ping-service/actions"
	"gitlab.com/livesocket/service/lib"
)

func main() {
	socket, err := lib.NewStandardSocket([]lib.Action{
		actions.PingAction,
	}, []lib.Subscription{})
	defer socket.Close()
	if err != nil {
		panic(err)
	}

	// Wait for CTRL-c or client close while handling events.
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	select {
	case <-sigChan:
	case <-socket.Client.Done():
		log.Print("Router gone, exiting")
		return
	}

}
