package actions

import (
	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/service/lib"
)

// PingAction Accepts pings
//
// public.ping
//
// Returns nothing
var PingAction = lib.Action{
	Proc:    "public.ping",
	Handler: ping,
}

func ping(invocation *wamp.Invocation) client.InvokeResult {
	return client.InvokeResult{}
}
