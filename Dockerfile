FROM golang:alpine as base
RUN apk --no-cache add git ca-certificates g++
WORKDIR /repos/ping-service
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/ping-service
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o ping-service

FROM scratch as release
COPY --from=builder /repos/ping-service/ping-service /ping-service
EXPOSE 8080
ENTRYPOINT ["/ping-service"]

